============================

To run this game on linux :

Install pygame with pip: pip install pygame.

Make the python file executable and run it to play, or just call python3 Zepplinator.py

To run this game on windows :

Just double click the Zepplinator.exe file :)

How to play:

Move with arrow keys and pause with escape, drop bombs with spacebar.

On linux machines, this game may change your screen resolution even after it is closed.

Good luck !

pictures directory contains picture of the gameplay to give you an idea of how it works without having to install
